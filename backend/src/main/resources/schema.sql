DROP TABLE IF EXISTS `project_generation_task`, `role`, `celonis_user`, `user_role`, `task_type`;

CREATE TABLE `task_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `project_generation_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `x_value` int(10) NOT NULL DEFAULT 0,
  `y_value` int(10) NOT NULL DEFAULT 0,
  `status` int NOT NULL,
  `type_id` int NOT NULL,
  `creation_date` DATE DEFAULT NULL,
  `storage_location` varchar(255) DEFAULT NULL,
  CONSTRAINT FKType_ID FOREIGN KEY (`type_id`) REFERENCES `task_type` (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `celonis_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  CONSTRAINT FKUser_ID FOREIGN KEY (`user_id`) REFERENCES `celonis_user` (`id`),
  CONSTRAINT FKRole_ID FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
);