INSERT INTO role (role_name, description) VALUES ('STANDARD_USER', 'Standard');
INSERT INTO role (role_name, description) VALUES ('ADMIN_USER', 'Admin');

INSERT INTO celonis_user (first_name, last_name, password, username) VALUES ('User', 'User', 'a58782fbc3ee87230841173cef58353dd814ab486f27b5264b56f04b97409d04', 'user');
INSERT INTO celonis_user (first_name, last_name, password, username) VALUES ('Admin', 'Admin', 'a58782fbc3ee87230841173cef58353dd814ab486f27b5264b56f04b97409d04', 'admin');

INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);

INSERT INTO task_type(`id`, `name`, `description`) VALUES (1, 'Generate sample project task', 'This generates the simple project that should be used to finish challenge 2 and 3.');
INSERT INTO task_type(`id`, `name`, `description`) VALUES (2, 'Count from X to Y', 'This task counts from given X to given Y.');

INSERT INTO project_generation_task(`x_value`, `y_value`, `type_id`, `status`, `creation_date`, `storage_location`) VALUES (1, 10, 2, 1, '2019-08-17', 'disk');
INSERT INTO project_generation_task(`x_value`, `y_value`, `type_id`, `status`, `creation_date`, `storage_location`) VALUES (1, 10, 2, 2, '2019-08-15', 'disk');
INSERT INTO project_generation_task(`x_value`, `y_value`, `type_id`, `status`, `creation_date`, `storage_location`) VALUES (1, 20, 2, 4, '2019-08-22', 'disk');
INSERT INTO project_generation_task(`x_value`, `y_value`, `type_id`, `status`, `creation_date`, `storage_location`) VALUES (1, 30, 2, 4, '2019-08-19', 'disk');


