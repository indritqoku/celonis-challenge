package com.celonis.challenge.services;

import com.celonis.challenge.exceptions.InternalException;
import com.celonis.challenge.exceptions.NotFoundException;
import com.celonis.challenge.model.ProjectGenerationTask;
import com.celonis.challenge.model.TaskStatus;
import com.celonis.challenge.model.TaskType;
import com.celonis.challenge.repository.ProjectGenerationTaskRepository;
import com.celonis.challenge.repository.TaskTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class TaskService {

    private final Logger log = LoggerFactory.getLogger(TaskService.class);

    private ProjectGenerationTaskRepository projectGenerationTaskRepository;

    private TaskTypeRepository taskTypeRepository;

    private FileService fileService;

    @Autowired
    public TaskService(ProjectGenerationTaskRepository projectGenerationTaskRepository,
                       TaskTypeRepository taskTypeRepository) {
        this.projectGenerationTaskRepository = projectGenerationTaskRepository;
        this.taskTypeRepository = taskTypeRepository;
    }

    public List<TaskType> listTypes() {
        return taskTypeRepository.findAll();
    }

    public List<ProjectGenerationTask> listTasks() {
        return projectGenerationTaskRepository.findAll();
    }

    public ProjectGenerationTask createTask(ProjectGenerationTask projectGenerationTask) {
        projectGenerationTask.setCreationDate(new Date());
        return projectGenerationTaskRepository.save(projectGenerationTask);
    }

    public ProjectGenerationTask getTask(Long taskId) {
        ProjectGenerationTask projectGenerationTask = projectGenerationTaskRepository.findOne(taskId);

        if (projectGenerationTask == null) {
            throw new NotFoundException();
        }
        return projectGenerationTask;
    }

    public void executeCountingTask(ProjectGenerationTask task) {
        if (task != null) {
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            executor.scheduleWithFixedDelay(() -> {
                if (task.isCanceled()) {
                    task.setCurrentValue(0);
                    task.setStatus(TaskStatus.EXECUTABLE);
                    update(task.getId(), task);
                    executor.shutdown();
                }
                task.setCurrentValue(task.getCurrentValue() + 1);
                if (task.getCurrentValue() > task.getyValue()) {
                    task.setStatus(TaskStatus.FINISHED);
                    update(task.getId(), task);
                    executor.shutdown();
                }
            }, 0, 1, TimeUnit.SECONDS);
        }
    }

    public ProjectGenerationTask update(Long taskId, ProjectGenerationTask projectGenerationTask) {
        ProjectGenerationTask result = projectGenerationTaskRepository.findOne(taskId);

        if (projectGenerationTask == null) {
            throw new NotFoundException();
        }

        result.setStatus(projectGenerationTask.getStatus());

        if (projectGenerationTask.getxValue() >= projectGenerationTask.getyValue()) {
            throw new InternalException("X value bigger then Y value.");
        }

        result.setxValue(projectGenerationTask.getxValue());
        result.setyValue(projectGenerationTask.getyValue());

        return projectGenerationTaskRepository.save(result);
    }

    public void delete(Long taskId) {
        projectGenerationTaskRepository.delete(taskId);
    }

    public void executeTask(Long taskId) {
        URL url = Thread.currentThread().getContextClassLoader().getResource("challenge.zip");
        if (url == null) {
            throw new InternalException("Zip file not found");
        }
        try {
            fileService.storeResult(taskId, url);
        } catch (Exception e) {
            throw new InternalException(e);
        }
    }


    public ProjectGenerationTask cancelTask(Long taskId) {
        ProjectGenerationTask projectGenerationTask = projectGenerationTaskRepository.findOne(taskId);

        if (projectGenerationTask == null) {
            throw new NotFoundException();
        }
        projectGenerationTask.setStatus(TaskStatus.EXECUTABLE);
        return projectGenerationTaskRepository.save(projectGenerationTask);
    }

    void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    /**
     * Not executed tasks should be automatically deleted after 7 days.
     * <p>
     * This is scheduled to get fired every monday at 1 AM.
     */
    @Scheduled(cron = "0 0 1 * * MON")
    public void removeNotActivatedUsers() {
        List<ProjectGenerationTask> tasks = projectGenerationTaskRepository
                .findAllByStatusAndCreationDateBefore(TaskStatus.EXECUTABLE, Date.from(Instant.now().minus(7, ChronoUnit.DAYS)));
        for (ProjectGenerationTask task : tasks) {
            log.debug("Deleting not executed task {}", task.getId());
            projectGenerationTaskRepository.delete(task);
        }
    }
}
