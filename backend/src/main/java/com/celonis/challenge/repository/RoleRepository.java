package com.celonis.challenge.repository;

import com.celonis.challenge.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
}
