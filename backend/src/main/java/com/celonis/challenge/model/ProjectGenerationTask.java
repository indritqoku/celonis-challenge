package com.celonis.challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "project_generation_task")
public class ProjectGenerationTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "x_value")
    private int xValue;

    @Column(name = "y_value")
    private int yValue;

    @Transient
    private int currentValue;

    @Transient
    private boolean canceled;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "storage_location")
    @JsonIgnore
    private String storageLocation;

    @Convert(converter = TaskStatusConverter.class)
    private TaskStatus status;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private TaskType taskType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getxValue() {
        return xValue;
    }

    public void setxValue(int xValue) {
        this.xValue = xValue;
    }

    public int getyValue() {
        return yValue;
    }

    public void setyValue(int yValue) {
        this.yValue = yValue;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectGenerationTask that = (ProjectGenerationTask) o;
        return xValue == that.xValue &&
                yValue == that.yValue &&
                currentValue == that.currentValue &&
                canceled == that.canceled &&
                Objects.equals(id, that.id) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(storageLocation, that.storageLocation) &&
                status == that.status &&
                Objects.equals(taskType, that.taskType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, xValue, yValue, currentValue, canceled, creationDate, storageLocation, status, taskType);
    }
}
