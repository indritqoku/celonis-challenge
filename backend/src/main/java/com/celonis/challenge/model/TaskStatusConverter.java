package com.celonis.challenge.model;

import javax.persistence.AttributeConverter;

/*Reference code from: http://eloquentdeveloper.com/2016/07/18/persisting-enums-using-jpa-attribute-converter/ */
public class TaskStatusConverter implements AttributeConverter<TaskStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(TaskStatus status) {
        if (null == status) {
            return null;
        }

        return status.getId();
    }

    @Override
    public TaskStatus convertToEntityAttribute(Integer id) {
        return TaskStatus.fromId(id);
    }
}
