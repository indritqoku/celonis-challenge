import {TaskType} from "./task.type";
import {TaskStatus} from "./task.status";

export class Task {
  public id: number;
  public xValue: number;
  public yValue: number;
  public currentValue: number;
  public status: TaskStatus;
  public creationDate: Date;
  public storageLocation: string;
  public taskType: TaskType;
  constructor() {}
}
